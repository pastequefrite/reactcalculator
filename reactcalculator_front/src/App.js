import React from 'react';

import {Route, Switch} from 'react-router-dom';
import routes from './navigation/routes';
import AuthenticatedRoute from './navigation/AuthenticatedRoute';

import {Container} from 'react-bootstrap';
import {StyleSheet, css} from 'aphrodite';

import Header from './components/Header';
import Footer from './components/Footer';

import {connect} from "react-redux";

class App extends React.Component {
    constructor(props) {
        super(props);

        if (!this.props.isLogged) {
            localStorage.removeItem('token');
        }
    }

    render() {
        return (
            <Container fluid className={css(styles.container)}>
                <Header/>
                <div className={css(styles.subContainer)}>
                    <Switch>
                        {routes.map(({key, path, Component, exact, allowAnonymous}) => (
                            allowAnonymous ?
                                <Route key={key} exact={exact} path={path} component={Component}/>
                                :
                                <AuthenticatedRoute key={key} exact path={path} component={Component}/>
                        ))}
                    </Switch>
                </div>
                <Footer/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 0,
        margin: 0,
    },
    subContainer: {
        paddingTop: 25,
        paddingBottom: 25,
    },
});

const mapStateToProps = state => {
    return {isLogged: state.isLogged};
};

export default connect(mapStateToProps)(App);
