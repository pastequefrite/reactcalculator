import React from 'react';
import {Route, Redirect} from 'react-router-dom';

const AuthenticatedRoute = ({component: children, ...rest}) => (
    <Route
        {...rest}
        render={({ location }) =>
            localStorage.getItem('token') ? (
                children
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: location }
                    }}
                />
            )
        }
    />
);

export default AuthenticatedRoute;
