import operation from './operation';

const initialState = {
    total: 0,
    prev: null,
    operator: null,
    sameNumber: false,
    operations: [],
    opId: 0,
};


export const calcul = (state = initialState, buttonName) => {
        let nextState;

        // Si c'est un nombre
        if (Number.isInteger(parseInt(buttonName))) {
            // si c'est le même on ajoute
            if (state.sameNumber) {
                nextState = {
                    ...state,
                    total: state.total + buttonName,
                };
                // sinon on écrase
            } else {
                nextState = {
                    ...state,
                    total: buttonName,
                    sameNumber: true,
                };
            }
        } else {
            // c'est autre chose
            switch (buttonName) {
                case "C":
                    // on remet tout à zéro
                    nextState = {
                        total: 0,
                        prev: null,
                        operator: null,
                        sameNumber: false,
                        operations: [],
                    };
                    break;
                case "+/-":
                    // change le signe si total different de zero
                    if (state.total !== 0) {
                        nextState = {
                            ...state,
                            total: state.total * -1,
                        };
                    }
                    break;
                case "%":
                    // divise par 100 si total different de zero
                    if (state.total !== 0) {
                        nextState = {
                            ...state,
                            total: state.total / 100,
                        };
                    }
                    break;
                case ".":
                    // ajout point si pas de point
                    if (!state.total.includes('.'))
                        nextState = {
                            ...state,
                            total: state.total + '.',
                        };
                    break;
                case "=":
                    if (state.total && state.prev && state.operator) {
                        nextState = {
                            ...state,
                            total: operation(state.prev, state.total, state.operator),
                            operations: [...state.operations, {
                                key: state.opId,
                                res: state.prev + state.operator + state.total
                            }],
                            operator: null,
                            opId: state.opId + 1,
                            sameNumber: false,
                        };
                    }
                    break;
                default:
                    //Appelé lors du clique sur les boutons opérations

                    // Nouveau nombre + enregistrement nouvel opérateur
                    nextState = {
                        ...state,
                        sameNumber: false,
                        operator: buttonName,
                    };

                    // si operateur -> effectuer operation et récupération du resultat
                    if (state.operator) {
                        const res = operation(state.prev, state.total, state.operator);

                        nextState = {
                            ...nextState,
                            total: res,
                            prev: res,
                            operations: [...state.operations, {
                                key: state.opId,
                                res: state.prev + state.operator + state.total
                            }],
                            opId: state.opId + 1
                        };
                    } else {
                        nextState = {
                            ...nextState,
                            prev: state.total,
                        };
                    }
                    break;

            }
        }
        return nextState || state;
    }
;
