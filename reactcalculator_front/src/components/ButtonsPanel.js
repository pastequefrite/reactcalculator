import React from 'react';

import {css, StyleSheet} from 'aphrodite';

import Button from './Button';
import PropTypes from "prop-types";


class ButtonsPanel extends React.Component {

    _onClick = (buttonName) => {
        this.props.onClick(buttonName)
    };

    render() {
        return (
            <div className={css(styles.container)}>
                <div className={css(styles.subContainer)}>
                    <Button name={'C'} onClick={this._onClick} color={'grey'}/>
                    <Button name={'+/-'} onClick={this._onClick} color={'grey'}/>
                    <Button name={'%'} onClick={this._onClick} color={'grey'}/>
                    <Button name={'/'} onClick={this._onClick} sustain={true}/>
                </div>
                <div className={css(styles.subContainer)}>
                    <Button name={'7'} onClick={this._onClick} color={'black'}/>
                    <Button name={'8'} onClick={this._onClick} color={'black'}/>
                    <Button name={'9'} onClick={this._onClick} color={'black'}/>
                    <Button name={'*'} onClick={this._onClick} sustain={true}/>
                </div>
                <div className={css(styles.subContainer)}>
                    <Button name={'4'} onClick={this._onClick} color={'black'}/>
                    <Button name={'5'} onClick={this._onClick} color={'black'}/>
                    <Button name={'6'} onClick={this._onClick} color={'black'}/>
                    <Button name={'-'} onClick={this._onClick} sustain={true}/>
                </div>
                <div className={css(styles.subContainer)}>
                    <Button name={'1'} onClick={this._onClick} color={'black'}/>
                    <Button name={'2'} onClick={this._onClick} color={'black'}/>
                    <Button name={'3'} onClick={this._onClick} color={'black'}/>
                    <Button name={'+'} onClick={this._onClick} sustain={true}/>
                </div>
                <div className={css(styles.subContainer)}>
                    <Button name={'0'} onClick={this._onClick} color={'black'} zeroBtn={true}/>
                    <Button name={'.'} onClick={this._onClick} color={'black'}/>
                    <Button name={'='} onClick={this._onClick}/>
                </div>
            </div>
        )
    }
}

ButtonsPanel.propTypes = {
    onClick: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
    },
    subContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    }
});

export default ButtonsPanel
