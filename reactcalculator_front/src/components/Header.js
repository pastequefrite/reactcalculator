import React from 'react';

import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';

import {Navbar, Nav} from 'react-bootstrap';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';

import {faCalculator, faSignOutAlt, faUser} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


class Header extends React.Component {

    render() {
        return (
            <Navbar bg="dark" variant="dark" expand="lg" className={css(styles.navBar)}>
                <Nav>
                    <Nav.Link as={NavLink} to="/" exact>React Calculator <FontAwesomeIcon
                        icon={faCalculator}/></Nav.Link>
                </Nav>

                <Navbar.Toggle aria-controls="basic-navbar-nav"/>

                <Navbar.Collapse id="basic-navbar-nav">
                    {this.props.isLogged ?
                        <Nav className="ml-auto">
                            <Nav.Link as={NavLink} to="/account" exact>
                                My account <FontAwesomeIcon icon={faUser}/>
                            </Nav.Link>
                            <Nav.Link as={NavLink} to="/logout" exact>
                                Sign out <FontAwesomeIcon icon={faSignOutAlt}/>
                            </Nav.Link>
                        </Nav>
                        : null
                    }
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

const styles = StyleSheet.create({
    navBar: {
        backgroundColor: theme.PRIMARY_COLOR,
    }
});

const mapStateToProps = state => {
    return {isLogged: state.isLogged};
};

export default connect(mapStateToProps)(Header);

