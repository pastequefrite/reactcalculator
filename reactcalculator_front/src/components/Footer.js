import React from 'react';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';

import {faGitlab, faYoutube, faFacebook} from '@fortawesome/free-brands-svg-icons';
import SocialIcons from "./SocialIcons";

class Footer extends React.Component {

    render() {
        return(
            <div className={css(styles.container)}>
                <div className={css(styles.social)}>
                    <SocialIcons icon={faGitlab} link={'https://gitlab.com'}/>
                    <SocialIcons icon={faYoutube} link={'https:youtube.com'}/>
                    <SocialIcons icon={faFacebook} link={'https://facebook.com'}/>
                </div>
                <div className={css(styles.copyLeft)}>Copyleft &copy; {(new Date().getFullYear())} CodeWhile </div>
            </div>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.PRIMARY_COLOR,
        padding: 10,
    },
    social: {
        display: 'flex',
        justifyContent: 'center',
    },
    copyLeft: {
        color: theme.LIGHT_COLOR,
        textAlign: 'center',
    },
});

export default Footer
