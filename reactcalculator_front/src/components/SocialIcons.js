import React from 'react';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';

import PropTypes from 'prop-types';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

class SocialIcons extends React.Component {

    render() {
        return (
            <a className={css(styles.link)} href={this.props.link}>
                <FontAwesomeIcon icon={this.props.icon}
                                 size={this.props.iconSize ? this.props.iconSize : '2x'}/>
            </a>
        )
    }
}

SocialIcons.propTypes = {
    icon: PropTypes.object.isRequired,
    link: PropTypes.string.isRequired,
    iconSize: PropTypes.string,
};

const styles = StyleSheet.create({
    link: {
        color: theme.LIGHT_COLOR,
        padding: 10,
        ':hover': {
            color: theme.SECONDARY_COLOR,
        }
    }
});

export default SocialIcons
