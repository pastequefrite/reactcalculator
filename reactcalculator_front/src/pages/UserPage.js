import React, {useState} from 'react';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';

import {Container} from 'react-bootstrap';
import UserForm from "../components/UserForm";

import {editUserInfos} from '../back/fakeApi';


function UserPage() {
    const [globalError, setGlobalError] = useState('');
    const [globalInfo, setGlobalInfo] = useState('');

    // After Validations and
    // If no error on registration : redirect to the login page.
    // Else : Print error
    const onSubmit = (data) => {
        setGlobalError('');
        setGlobalInfo('');

        editUserInfos({...data, token : localStorage.getItem('token')})
            .then(() => setGlobalInfo("Changes has been saved !"))
            .catch(error => {
                setGlobalError(error.toString())
            })
    };

    return (
        <Container>
            <h1 className={css(styles.title)}>User Infos</h1>
            <UserForm globalError={globalError} globalInfo={globalInfo} onSubmit={onSubmit} />
        </Container>
    )
}

const styles = StyleSheet.create({
    title: {
        color: theme.SECONDARY_COLOR,
    },
});

export default UserPage
