import React from 'react'
import { connect } from 'react-redux'
import {logout} from '../store/actions';
import {Redirect} from 'react-router-dom';

class LogoutPage extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(logout());
        localStorage.removeItem('token');
    }

    render() {
        return (
            <Redirect to="/" />
        );
    }
}

export default connect()(LogoutPage);
