import React, {useState} from 'react';

import {NavLink, useHistory, useLocation} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {Form, Container, Button} from 'react-bootstrap';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';

import useForm from 'react-hook-form';
import InputForm from '../components/InputForm';

import {login} from '../back/fakeApi';
import {login as loginAction} from '../store/actions';


function LoginPage() {
    const {register, handleSubmit, errors} = useForm();

    const [globalError, setGlobalError] = useState('');
    const [showPass, setShowPass] = useState(false);

    const dispatch = useDispatch();
    let history = useHistory();
    let location = useLocation();
    let {from} = location.state || {from: {pathname: "/"}};

    // After Validations and
    // If credentials are valid : redirect to the page that user wanted to access earlier.
    // Else : Print "Invalid credentials"
    const onSubmit = (data) => {
        if (login(data)) {
            dispatch(loginAction());
            history.replace(from);
        } else {
            setGlobalError("Invalid credentials");
        }
    };

    const checkErrors = (errors) => {
        if (errors) {
            switch (errors.type) {
                case 'required':
                    return "This field is required";
                case 'maxLength':
                    return "This field is too long";
                case 'minLength':
                    return "This field is too short";
                case 'pattern':
                    return "This field is invalid";
                case 'validate':
                    return "Password and confirm password do not match";
                default:
                    break;
            }
        }
    };

    return (
        <Container>
            <h1 className={css(styles.title)}>Sign in</h1>
            <Form className={css(styles.form)}>
                <Form.Control.Feedback type="invalid" className={css(styles.feedBack)}>
                    {globalError}
                </Form.Control.Feedback>


                {/* USERNAME INPUT */}
                <InputForm required
                           name="username"
                           label="Username"
                           feedBackText={checkErrors(errors.username)}
                           register={register({required: true})}
                />

                {/* PASSWORD INPUT */}
                <InputForm required
                           name="password"
                           label="Password"
                           isPassword={!showPass}
                           feedBackText={checkErrors(errors.password)}
                           register={register({required: true})}
                           inputGroup={
                               <Button onClick={() => setShowPass(!showPass)}
                                       tabIndex="-1"
                                       className={css(styles.button)}><FontAwesomeIcon
                                   icon={showPass ? faEyeSlash : faEye}/></Button>
                           }
                />

                <div className={css(styles.buttonContainer)}>
                    <Button type="submit" className={css(styles.button)}
                            onClick={handleSubmit(onSubmit)}>Sign in</Button>
                </div>
                <div className={css(styles.linkContainer)}>
                    <NavLink to="/register">No account ?</NavLink>
                </div>
            </Form>
        </Container>
    )
}

const styles = StyleSheet.create({
    title: {
        color: theme.SECONDARY_COLOR,
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: theme.PRIMARY_COLOR,
        borderColor: theme.PRIMARY_COLOR,
    },
    linkContainer: {
        // width: '100%',
        textAlign: 'center',
    },
    form: {
        maxWidth: 500,
        margin: 'auto',
    },
    feedBack: {
        display: 'flex',
    }
});


export default LoginPage;
