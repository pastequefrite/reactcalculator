import React, {useState} from 'react';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';

import {Container} from 'react-bootstrap';
import UserForm from '../components/UserForm';
import Modal from '../components/Modal';

import {register as registerUser} from '../back/fakeApi';


function RegisterPage() {
    const [globalError, setGlobalError] = useState('');
    const [openModal, setOpenModal] = useState(false);


    const toggleModal = () => {
        setOpenModal(!openModal);
    };

    // After Validations and
    // If no error on registration : redirect to the login page.
    // Else : Print error
    const onSubmit = (data) => {
        setGlobalError('');

        registerUser(data)
            .then(() => toggleModal())
            .catch(error => {
                setGlobalError(error.toString())
            })
    };

    return (
        <Container>
            <h1 className={css(styles.title)}>Register</h1>
            <Modal open={openModal} toggle={toggleModal}/>
            <UserForm isRegistration globalError={globalError} onSubmit={onSubmit} />
        </Container>
    )
}

const styles = StyleSheet.create({
    title: {
        color: theme.SECONDARY_COLOR,
    },
});

export default RegisterPage
