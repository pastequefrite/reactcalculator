import React from 'react';

import {StyleSheet, css} from 'aphrodite';

import Calculator from '../components/Calculator';


class HomePage extends React.Component {

    render() {
        return (
            <div className={css(styles.calculatorContainer)}>
                <Calculator/>
            </div>
        )
    }
}

const styles = StyleSheet.create({
    calculatorContainer: {
        display: 'flex',
        justifyContent: 'center',
        padding: 50,
    },
});

export default HomePage
